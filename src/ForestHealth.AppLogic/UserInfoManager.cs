using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;
using ForestHealth.Domain.Repositories;

namespace ForestHealth.AppLogic
{
    public class UserInfoManager
    {
        public IUserInfoRepository UserInfoRepo { get; set; }

        public UserInfo CreateAndReturn(string name)
        {
            return null;
        }

        public UserInfo FetchById(int id)
        {
            return this.UserInfoRepo.FetchById(id);
        }

        public IEnumerable<UserInfo> GetAllUserInfos()
        {
            return this.UserInfoRepo.FetchAll();
        }
    }
}