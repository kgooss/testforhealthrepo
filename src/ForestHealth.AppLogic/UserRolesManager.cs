using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;
using ForestHealth.Domain.Repositories;

namespace ForestHealth.AppLogic
{
    public class UserRolesManager
    {
        public IUserRolesRepository UserRoleRepo { get; set; }

        public UserRoles FetchById(int id)
        {
            return this.UserRoleRepo.FetchById(id);
        }
        
        public IEnumerable<UserRoles> FetchAllRolesByUserInfoId(int userInfoId)
        {
            return this.UserRoleRepo.FetchAllRolesByUserInfoId(userInfoId);
        }
    }
}