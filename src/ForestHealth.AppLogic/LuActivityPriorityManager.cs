using ForestHealth.Domain.Entities;
using ForestHealth.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForestHealth.AppLogic
{
    public class LuActivityPriorityManager
    {
        public ILuActivityPriorityRepository APRepo { get; set; }

        public LuActivityPriority CreateAndReturn(string name)
        {
            var newId = this.APRepo.InsertAP(name);
            return this.APRepo.FetchById(newId);
        }

        public IEnumerable<LuActivityPriority> GetAllActivityPriorityObjects()
        {
            return this.APRepo.FetchAll();
        }
    }
}