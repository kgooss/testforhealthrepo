using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;
using ForestHealth.Domain.Repositories;

namespace ForestHealth.AppLogic
{
    public class UserMembershipDataManager
    {
        public IUserMembershipDataRepository UserMembershipDataRepo { get; set; }
        public UserMembershipData FetchById(int id)
        {
            return this.UserMembershipDataRepo.FetchById(id);
        }
        
    }
}