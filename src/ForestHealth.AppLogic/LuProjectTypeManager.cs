using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;
using ForestHealth.Domain.Repositories;

namespace ForestHealth.AppLogic
{
    public class LuProjectTypeManager
    {
        public ILuProjectTypeRepository LuProjectTypeRepo { get; set; }

        public LuProjectType FetchById(string id)
        {
            return this.LuProjectTypeRepo.FetchById(id);
        }

        public IEnumerable<LuProjectType> FetchAll()
        {
            return this.LuProjectTypeRepo.FetchAll();
        }
    }
}