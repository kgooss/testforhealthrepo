﻿using ForestHealth.Domain.Entities;
using ForestHealth.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForestHealth.AppLogic
{
    public class TestManager
    {
        public ITestRepository TestRepo { get; set; }

        public Test CreateAndReturn(string name)
        {
            var newId = this.TestRepo.InsertTest(name);
            return this.TestRepo.FetchById(newId);
        }

        public IEnumerable<Test> GetAllTestObjects()
        {
            return this.TestRepo.FetchAll();
        }
    }
}