using ForestHealth.Domain.Entities;
using ForestHealth.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForestHealth.AppLogic
{
    public class LuCountyManager
    {
        public ILuCountyRepository CountyRepo { get; set; }

        public LuCounty CreateAndReturn(string Name, int CountyId, int StateId)
        {
            var newId = this.CountyRepo.InsertCounty(Name, CountyId, StateId);
            return this.CountyRepo.FetchById(newId);
        }

        public LuCounty FetchById(int id)
        {
            return this.CountyRepo.FetchById(id);
        }

        public IEnumerable<LuCounty> GetAllCountyObjects()
        {
            return this.CountyRepo.FetchAll();
        }


    }
}