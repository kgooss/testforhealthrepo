using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;
using ForestHealth.Domain.Repositories;

namespace ForestHealth.AppLogic
{
    public class UserInfoAggManager
    {
        public IUserInfoAggRepository UserInfoAggRepo { get; set; }

        public UserInfoAgg CreateAndReturn(string name)
        {
            return null;
        }

        public UserInfoAgg FetchById(int id)
        {
            return this.UserInfoAggRepo.FetchById(id);
        }

        public IEnumerable<UserInfoAgg> GetAllUserInfos()
        {
            return this.UserInfoAggRepo.FetchAll();
        }

        public IEnumerable<UserInfoAgg> FetchAllUsersByEauthId(string eauthId)
        {
            return this.UserInfoAggRepo.FetchAllUsersByEauthId(eauthId);
        }
    }
}