using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;
using ForestHealth.Domain.Repositories;

namespace ForestHealth.AppLogic
{
    public class OrganizationManager
    {
        public IOrganizationRepository OrganizationRepo { get; set; }

        public Organization FetchById(int id)
        {
            return this.OrganizationRepo.FetchById(id);
        }

        public IEnumerable<Organization> FetchAll()
        {
            return this.OrganizationRepo.FetchAll();
        }

        public IEnumerable<Organization> FetchAllFSRegions()
        {
            return this.OrganizationRepo.FetchAllFSRegions();
        }
    }
}