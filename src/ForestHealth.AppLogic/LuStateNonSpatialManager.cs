using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;
using ForestHealth.Domain.Repositories;

namespace ForestHealth.AppLogic
{
    public class LuStateNonSpatialManager
    {
        public ILuStateNonSpatialRepository LuStateNonSpatialRepo { get; set; }

        public LuStateNonSpatial FetchById(int id)
        {
            return this.LuStateNonSpatialRepo.FetchById(id);
        }
    }
}