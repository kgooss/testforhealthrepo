﻿using System;
using System.Web;
using System.Web.Mvc;
using ForestHealth.Web.Utilities;

namespace ForestHealth.Web.Helpers
{
    public class UserInfoCookieModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (controllerContext == null)
                throw new ArgumentNullException("controllerContext");
            if (bindingContext == null)
                throw new ArgumentNullException("bindingContext");

            UserInfoCookie userInfoCookie = FormsAuthenticationCookieHelper.GetUserInfoCookie();

            if (userInfoCookie != null)
            {
                userInfoCookie.Roles = GetUserRoles(userInfoCookie.EAuthId);
            }

            return userInfoCookie;
        }

        private string[] GetUserRoles(string eauthid)
        {
            // retrieve roles catched by Application_AuthenticateRequest (in Global.asax.cs)
            object rolesFromCache = HttpRuntime.Cache.Get(eauthid);
            if (rolesFromCache != null)
            {
                return rolesFromCache as string[];
            }

            return null;
        }
    }
}