﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using System.Runtime.InteropServices;

namespace ForestHealth.Web.Helpers
{
    [Serializable]
    [ComVisible(true)]
    public class SMARTPrincipal : MarshalByRefObject, IPrincipal
    {
        public List<string> Roles { get; set; }
        public IIdentity Identity { get; set; }

        public SMARTPrincipal()
        {
            Identity = new SMARTIdentity();
            Roles = new List<string>();
        }

        public bool IsInRole(string role)
        {
            return Roles.Contains(role);
        }
    }
}