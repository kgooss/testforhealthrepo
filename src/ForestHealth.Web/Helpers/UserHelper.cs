﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ForestHealth.Web.Helpers
{
    public class UserHelper
    {
        //todo: consider adding a new attribute to the database to determine this (instead of null stateid)
        public static bool IsUSFSUser(int? organizationStateId)
        {
            return organizationStateId == null;
        }

        //todo: consider adding a new attribute to the database to determine this (instead of parsing a string)
        public static bool IsRegionalUSFS(string organizationName)
        {
            return !string.IsNullOrWhiteSpace(organizationName)
                && organizationName.StartsWith("USDA Forest Service - ", StringComparison.CurrentCultureIgnoreCase);
        }
    }
}