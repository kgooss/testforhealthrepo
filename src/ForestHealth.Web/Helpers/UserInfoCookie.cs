﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace ForestHealth.Web.Helpers
{
    [DataContract]
    public class UserInfoCookie
    {
        public UserInfoCookie()
        {
            Roles = new string[0];
        }

        [DataMember]
        public String DisplayName { get; set; }
        [DataMember]
        public int? OrganizationId { get; set; }
        [DataMember]
        public int? OrganizationStateId { get; set; }
        [DataMember]
        public string OrganizationName { get; set; }
        [DataMember]
        public string OrganizationStateAbbv { get; set; }
        [DataMember]
        public int UserId { get; set; }

        // no longer storing roles in cookie due to size limit being exceeded
        [XmlIgnore]
        [DataMember]
        public string[] Roles { get; set; }

        [DataMember]
        public string EAuthId { get; set; }
        [DataMember]
        public DateTime? ApprovalDate { get; set; }
        [DataMember]
        public DateTime? DeactivationDate { get; set; }
        [DataMember]
        public string AreaAffiliation { get; set; }

        public virtual bool IsUSFSUser
        {
            get { return UserHelper.IsUSFSUser(OrganizationStateId); }
        }

        public virtual bool IsRegionalUSFS
        {
            get { return UserHelper.IsRegionalUSFS(OrganizationName); }
        }
    }
}