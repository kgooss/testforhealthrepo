﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using System.Runtime.InteropServices;

namespace ForestHealth.Web.Helpers
{
    [Serializable]
    [ComVisible(true)]
    public class SMARTIdentity : MarshalByRefObject, IIdentity
    {
        public string Name
        {
            get
            {
                return UserId.HasValue ? UserId.ToString() : String.Empty;
            }
        }
        public string AuthenticationType { get; set; }

        //SMART specific
        public int? UserId { get; set; }
        public string DisplayName { get; set; }
        public string EAuthId { get; set; }
        public int? OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public int? OrganizationStateId { get; set; }
        public string OrgStateAbbr { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public DateTime? DeactivationDate { get; set; }
        public string AreaAffiliation { get; set; }

        public SMARTIdentity()
        {
            AuthenticationType = "SMART";
            ApprovalDate = null;
            DeactivationDate = null;
        }

        public bool IsAuthenticated
        {
            get { return UserId != null; }
        }

        public bool IsUSFSUser
        {
            get { return UserHelper.IsUSFSUser(OrganizationStateId); }
        }

        public bool IsRegionalUSFS
        {
            get { return UserHelper.IsRegionalUSFS(OrganizationName); }
        }
    }
}