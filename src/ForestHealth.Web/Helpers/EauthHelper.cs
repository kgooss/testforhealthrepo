﻿using ForestHealth.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ForestHealth.Web.Helpers
{
    public class DevEauthHelper : IEauthHelper
    {
        #region IEauthHelper Members

        private ProdEauthHelper ProdEauthHelper { get; set; }

        public DevEauthHelper()
        {
            this.ProdEauthHelper = new ProdEauthHelper();
        }

        public string GetEauthId()
        {
            String prodEauthId = this.ProdEauthHelper.GetEauthId();

            if (!String.IsNullOrEmpty(prodEauthId))
            {
                return prodEauthId;
            }

            if (HttpContext.Current.Request.Cookies["HTTP_USDAEAUTHID"] != null)
            {
                return HttpContext.Current.Request.Cookies["HTTP_USDAEAUTHID"].Value;
            }
            return string.Empty;
        }

        #endregion
    }
}