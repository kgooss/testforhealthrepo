﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ForestHealth.Web.Helpers
{
    public class ProdEauthHelper : IEauthHelper
    {
        public static readonly String EauthIdHeaderName = "usdaeauthid";

        #region IEauthHelper Members

        public string GetEauthId()
        {
            if (null != HttpContext.Current.Request.Headers[ProdEauthHelper.EauthIdHeaderName])
            {
                return HttpContext.Current.Request.Headers[ProdEauthHelper.EauthIdHeaderName];
            }
            return string.Empty;
        }

        #endregion
    }
}