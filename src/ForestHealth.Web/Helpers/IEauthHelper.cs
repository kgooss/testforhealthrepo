﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ForestHealth.Web.Helpers
{
    public interface IEauthHelper
    {
        string GetEauthId();
    }
}