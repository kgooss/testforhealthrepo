﻿using Castle.Windsor;
using ForestHealth.Domain.Entities;
using ForestHealth.Domain.Repositories;
using ForestHealth.Persistence.Oracle.Repositories;
using ForestHealth.Persistence.Oracle.UnitOfWork;
using ForestHealth.Web.App_Start;
using ForestHealth.Web.Helpers;
using ForestHealth.Web.Utilities;
using ForestHealth.Web.Utilities.IoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ForestHealth.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static WindsorContainer Container = new WindsorContainer();

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            IoCConfig.RegisterComponents(MvcApplication.Container);

            ModelBinders.Binders.Add(typeof(UserInfoCookie), new UserInfoCookieModelBinder());


            ControllerBuilder.Current.SetControllerFactory(new WindsorControllerFactory(MvcApplication.Container));
        }

        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.Url.PathAndQuery.ToLower().Contains("favicon.ico"))
            {
                return;
            }

            //Check if the forms authentication cookie is already present
            //If so, get user info from that and set principal on the request
            var cookie = FormsAuthenticationCookieHelper.GetUserInfoCookie();
            string eauthid = new DevEauthHelper().GetEauthId();

            if (cookie != null)
            {
                if (cookie.EAuthId == eauthid)
                {
                    cookie.Roles = GetUserRoles(eauthid);
                    HttpContext.Current.User = FormsAuthenticationCookieHelper.MakeSMARTPrincipal(cookie);
                    return;
                }
                FormsAuthenticationCookieHelper.ClearUserInfoCookie();
            }

            //If neither the identity on the request or the cookie for the session is available,
            //Get the user credentials from the database and create the identity and the cookie for it
            if (string.IsNullOrEmpty(eauthid))
            {
                HttpContext.Current.User = new SMARTPrincipal();
                return;
            }
            var userdto = GetUserInfoForEAuth(eauthid);

            if (userdto == null)
            {
                HttpContext.Current.User = new SMARTPrincipal()
                {
                    Identity = new SMARTIdentity() { EAuthId = eauthid }
                };
                return;
            }

            var info = FormsAuthenticationCookieHelper.CreateUserInfoCookie(userdto);

            FormsAuthenticationCookieHelper.SetUserInfoCookie(info);

            // no longer storing roles in cookie due to size limit being exceeded
            info.Roles = GetUserRoles(eauthid);
            HttpContext.Current.User = FormsAuthenticationCookieHelper.MakeSMARTPrincipal(info);
        }

        private string[] GetUserRoles(string eauthid)
        {
            var rolesFromCache = HttpRuntime.Cache.Get(eauthid);
            if (rolesFromCache != null)
            {
                return rolesFromCache as string[];
            }
            var user = GetUserInfoForEAuth(eauthid);

            string[] userRoles = user.userRoles.Select(r => r.Rolename).ToArray();

            HttpRuntime.Cache.Insert(eauthid, userRoles, null, DateTime.Now.AddMinutes(2), Cache.NoSlidingExpiration);

            return userRoles;
        }


        private static UserInfoAgg GetUserInfoForEAuth(string eauthid)
        {
            var userInfoAggRepo = Container.Resolve<IUserInfoAggRepository>();
            var availableUsers = userInfoAggRepo.FetchAllUsersByEauthId(eauthid);
            if (availableUsers.Count(u => u.ApprovalDate.HasValue && !u.DeactivationDate.HasValue) > 0)
            {
                availableUsers = availableUsers.Where(u => u.ApprovalDate.HasValue && !u.DeactivationDate.HasValue);
            }
            return availableUsers.OrderByDescending(u => u.userMembershipData.Lastlogindate).FirstOrDefault();
        }
    }
}