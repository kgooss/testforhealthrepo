﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ForestHealth.Web.Helpers;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Web.Security;
using ForestHealth.Domain.Entities;
//using BusinessLogic.DTO;

namespace ForestHealth.Web.Utilities
{
    public static class FormsAuthenticationCookieHelper
    {

        public static SMARTPrincipal MakeSMARTPrincipal(UserInfoCookie info)
        {
            return new SMARTPrincipal()
            {
                Identity = new SMARTIdentity()
                {
                    UserId = info.UserId,
                    EAuthId = info.EAuthId,
                    OrganizationId = info.OrganizationId,
                    OrganizationName = info.OrganizationName,
                    OrganizationStateId = info.OrganizationStateId,
                    OrgStateAbbr = info.OrganizationStateAbbv,
                    DisplayName = info.DisplayName,
                    ApprovalDate = info.ApprovalDate,
                    DeactivationDate = info.DeactivationDate,
                    AreaAffiliation = info.AreaAffiliation
                },
                Roles = new List<string>(info.Roles)
            };
        }

        public static void SetUserInfoCookie(UserInfoCookie info)
        {
            string userData = string.Empty;
            using (StringWriter writer = new StringWriter())
            {
                XmlSerializer serializer = new XmlSerializer(typeof(UserInfoCookie));
                serializer.Serialize(writer, info);
                userData = writer.ToString();
            }
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                1,
                info.UserId.ToString(),
                DateTime.Now,
                DateTime.Now.AddMinutes(30),
                false,
                userData,
                FormsAuthentication.FormsCookiePath);

            MembershipUser membershipUser = Membership.GetUser(info.UserId.ToString());
            membershipUser.LastLoginDate = DateTime.Now;

            Membership.UpdateUser(membershipUser);

            if (HttpContext.Current != null)
                HttpContext.Current.Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket)));
        }

        public static void ClearUserInfoCookie()
        {
            if (HttpContext.Current != null && HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                HttpContext.Current.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
        }

        public static UserInfoCookie GetUserInfoCookie()
        {
            if (HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName] == null)
                return null;

            var userInfo = new UserInfoCookie();
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName].Value);
            string s = ticket.UserData;
            userInfo = DeserializeFromXML(s);
            return userInfo;
        }

       
        public static UserInfoCookie CreateUserInfoCookie(UserInfoAgg userInfoAgg)
        {
            return new UserInfoCookie()
            {
                UserId = userInfoAgg.UserInfoId,
                EAuthId = userInfoAgg.EauthId,
                DisplayName = string.Format("{0} {1}", userInfoAgg.FirstName, userInfoAgg.LastName),
                OrganizationId = userInfoAgg.OrganizationId,
                OrganizationStateId = userInfoAgg.organization.StateId,
                Roles = userInfoAgg.userRoles.Select(r => r.Rolename).ToArray(),
                ApprovalDate = userInfoAgg.ApprovalDate,
                DeactivationDate = userInfoAgg.DeactivationDate,
                OrganizationName = userInfoAgg.organization.Name,
                OrganizationStateAbbv = userInfoAgg.luStateNonSpatial.Abbreviation,
                AreaAffiliation = userInfoAgg.AreaAffiliation
            };
        }
       

        static UserInfoCookie DeserializeFromXML(string s)
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(UserInfoCookie));
            StringReader stringReader = new StringReader(s);
            XmlTextReader xmlReader;
            xmlReader = new XmlTextReader(stringReader);
            UserInfoCookie userInfo = (UserInfoCookie)deserializer.Deserialize(xmlReader);
            xmlReader.Close();
            stringReader.Close();
            return userInfo;
        }
    }
}