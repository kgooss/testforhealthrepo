﻿using ForestHealth.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ForestHealth.Web.Models.OutputModels
{
    public class ForestHealthProject_CreateNewModel
    {
        //all possible project types
        public IEnumerable<LuProjectType> ProjectTypes { get; set; }
        //the user selection from the dropdown
        public LuProjectType UserEnteredProjectType { get; set; }
        public string UserEnteredProjectName { get; set; }
        public string UserEnteredAgency { get; set; }
        public IEnumerable<Organization> FSRegions { get; set; }
        public string UserEnteredFSRegion { get; set; }
        public string RegionalId { get; set; }
        public string PrimaryState { get; set; }
        public int FiscalYear { get; set; }
        public string ProjectPriority { get; set; }
        public string ProjectDescription { get; set; }
    }
}