﻿using ForestHealth.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ForestHealth.Web.Models.OutputModels
{
    public class ATestModel
    {
        public bool SomeThing { get; set; }
        public string aString { get; set; }
        public IDictionary<string,string> someCodes { get; set; }
        public LuCounty someCounty { get; set; }
    }
}