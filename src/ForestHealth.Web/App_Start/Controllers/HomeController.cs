﻿using ForestHealth.AppLogic;
using ForestHealth.Web.Helpers;
using ForestHealth.Web.Models.OutputModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ForestHealth.Web.Controllers
{
    public class HomeController : Controller
    {
        public UserInfoAggManager UserInfoAggManager { get; set; }

        public ActionResult Index(UserInfoCookie userInfoCookie)
        {
            /*
            string eAuthId = userInfoCookie.EAuthId;

            var availableUsers = UserInfoAggManager.FetchAllUsersByEauthId(eAuthId);
            if (availableUsers.Count(u => u.ApprovalDate.HasValue && !u.DeactivationDate.HasValue) > 0)
            {
                availableUsers = availableUsers.Where(u => u.ApprovalDate.HasValue && !u.DeactivationDate.HasValue);
            }
            var theUser = availableUsers.OrderByDescending(u => u.userMembershipData.Lastlogindate).FirstOrDefault();
            */
            return View("Index", new Home_IndexModel
            {
                //userInfoAgg = theUser
            });
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}