﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ForestHealth.Web.Models.OutputModels;
using ForestHealth.Web.Helpers;
using ForestHealth.Domain.Entities;
using ForestHealth.AppLogic;

namespace ForestHealth.Web.App_Start.Controllers
{
    public class ForestHealthProjectController : Controller
    {
        public LuProjectTypeManager LuProjectTypeManager { get; set; }
        public OrganizationManager OrganizationManager { get; set; }

        // GET: ForestHealthProject
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult CreateNew(UserInfoCookie userInfoCookie)
        {
            IEnumerable<LuProjectType> projectTypes = LuProjectTypeManager.FetchAll();
            IEnumerable<Organization> fsRegions = OrganizationManager.FetchAllFSRegions();

            return View("CreateNew", new ForestHealthProject_CreateNewModel
            {
                ProjectTypes = projectTypes,
                FSRegions = fsRegions
            });
        }
    }
}