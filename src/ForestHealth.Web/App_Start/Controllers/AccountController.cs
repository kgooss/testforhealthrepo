﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace ForestHealth.Web.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult FakeEAuth(string desiredId)
        {
            HttpContext.Response.Cookies.Remove("HTTP_USDAEAUTHID");
            FormsAuthentication.SignOut();

            var eAuthCookie = new HttpCookie("HTTP_USDAEAUTHID", desiredId);
            eAuthCookie.Expires = DateTime.Now.AddMinutes(60);
            HttpContext.Response.Cookies.Add(eAuthCookie);

            return new RedirectToRouteResult("Default", new RouteValueDictionary { { "controller", "Home" }, { "action", "Index" } });
        }
    }
}