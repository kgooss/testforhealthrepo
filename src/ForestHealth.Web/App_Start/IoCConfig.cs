﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using ForestHealth.AppLogic;
//using ForestHealth.Persistence.SqlServer.Repositories;
//using ForestHealth.Persistence.SqlServer.UnitOfWork;
using ForestHealth.Persistence.Oracle.Repositories;
using ForestHealth.Persistence.Oracle.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ForestHealth.Web.App_Start
{
    public class IoCConfig
    {
        public static void RegisterComponents(IWindsorContainer container)
        {
            container.Register(

                Classes.FromThisAssembly()
                    .BasedOn<IController>()
                    .Configure(c => c.LifestylePerWebRequest()),

                //Classes.FromAssemblyContaining<TestManager>()
                //    .Where(type => type.Name.EndsWith("Manager"))
                //    .Configure(c => c.LifestylePerWebRequest()),

                //Classes.FromAssemblyContaining<TestRepository>()
                //    .Where(type => type.Name.EndsWith("Repository"))
                //    .WithService
                //    .FirstInterface()
                //    .Configure(c => c.LifestylePerWebRequest()),

                 Classes.FromAssemblyContaining<LuActivityPriorityManager>()
                    .Where(type => type.Name.EndsWith("Manager"))
                    .Configure(c => c.LifestylePerWebRequest()),

                Classes.FromAssemblyContaining<ActivityPriorityRepository>()
                    .Where(type => type.Name.EndsWith("Repository"))
                    .WithService
                    .FirstInterface()
                    .Configure(c => c.LifestylePerWebRequest()),

                Component.For<IUnitOfWorkFactory>().ImplementedBy<UnitOfWorkFactory>()
                    .DependsOn(
                        Parameter.ForKey("useTransaction").Eq(true.ToString())
                    )
                    .LifestylePerWebRequest()
            );
        }
    }
}