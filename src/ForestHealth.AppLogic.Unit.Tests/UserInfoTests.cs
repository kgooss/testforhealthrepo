using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;
using ForestHealth.Domain.Repositories;
using NUnit.Framework;
using Moq;
using ForestHealth.AppLogic;

namespace ForestHealth.AppLogic.Unit.Tests
{
    [TestFixture]
    public class UserInfoTests
    {

        [Test]
        public void Test_FetchUserById_With_A_Valid_User_In_Database()
        {
            var returnObject = new UserInfo();
            Mock<IUserInfoRepository> mockUserInfoRepo = new Mock<IUserInfoRepository>();

            mockUserInfoRepo.Setup<UserInfo>(r => r.FetchById(0))
                .Returns(returnObject)
                .Verifiable();

            var results = new UserInfoManager()
            {
                UserInfoRepo = mockUserInfoRepo.Object
            }
            .FetchById(0);
         
            mockUserInfoRepo.Verify();
            Assert.IsTrue(object.ReferenceEquals(returnObject,results));

        }

        internal static UserInfo GetTestEntity()
        {
            UserInfo entity = new UserInfo()
            {
                UserInfoId = 9,
                FirstName = "test",
                LastName = "test",
                AddressCity = "testcity",
                AddressLine1 = "1234 test st",
                AddressStateId = 1,
                AddressZipCode = "12345",
                EauthId = "12345",
                EmailAddress = "test@test.com",
                Phone = "1234567891",
                OrganizationUnit = "test",
                OrganizationId = 9
                /*
                ,
                Organization = new Organization()
                {
                    Name = "VDOT",
                    StateId = 1,
                    LuStateNonSpatial = new LuStateNonSpatialEntity()
                    {
                        Abbreviation = "VA"
                    }
                },
                UserMembershipData = new UserMembershipDataEntity()
                {
                    Createdate = DateTime.MinValue,
                    Lastlogindate = DateTime.MaxValue
                },
                LuStateNonSpatial = new LuStateNonSpatialEntity()
                {
                    Abbreviation = "AA"
                }
                */
            };

            /*
            entity.UserRoles.Add(
                new UserRolesEntity()
                {
                    Rolename = "Bob Dole"
                }
            );
            */

            return entity;
        }
    }
}