using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;
using ForestHealth.Domain.Repositories;
using NUnit.Framework;
using Moq;


namespace ForestHealth.AppLogic.Unit.Tests
{
    [TestFixture]
    public class LuCountyManagerTests
    {
        [Test]
        public void FetchAllCounties_Test()
        {
            Mock<ILuCountyRepository> mockLuCountyRepo = new Mock<ILuCountyRepository>();

            mockLuCountyRepo.Setup(x => x.FetchAll())
                .Returns(new List<LuCounty>() { new LuCounty { CountyId = 1, Name = "Billy", StateId = 22 }, new LuCounty { CountyId = 2, Name = "Grumpy", StateId = 33 } })
                .Verifiable();

            var results = new LuCountyManager()
            {
                CountyRepo = mockLuCountyRepo.Object
            }
            .GetAllCountyObjects();

            mockLuCountyRepo.Verify();

            Assert.IsNotNull(results);
            var singleResult = results.First();
            Assert.IsNotNull(singleResult.Name);

        }

        [Test]
        public void FetchCountyById_Test()
        {
            var returnObject = new LuCounty();
            Mock<ILuCountyRepository> mockLuCountyRepo = new Mock<ILuCountyRepository>();

            mockLuCountyRepo.Setup(x => x.FetchById(2111))
                .Returns(returnObject)
                .Verifiable();

            var results = new LuCountyManager()
            {
                CountyRepo = mockLuCountyRepo.Object
            }
            .FetchById(2111);

            mockLuCountyRepo.Verify();

            Assert.IsTrue(object.ReferenceEquals(returnObject, results));

        }
    }
}