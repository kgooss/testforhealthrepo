﻿using ForestHealth.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForestHealth.Domain.Repositories
{
    public interface ITestRepository
    {
        int InsertTest(string name);
        Test FetchById(int id);
        IEnumerable<Test> FetchAll();
    }
}
