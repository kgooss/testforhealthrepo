using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;

namespace ForestHealth.Domain.Repositories
{
    public interface IUserInfoRepository
    {
        int InsertUserInfo(string name);
        UserInfo FetchById(int id);
        IEnumerable<UserInfo> FetchAll();

        IEnumerable<UserInfo> FetchAllUsersByOrgStateAbbrAndRoles(string stateAbbr, params string[] roles);

        IEnumerable<UserInfo> FetchAllUsersByEauthId(string eauthId);

    }
}