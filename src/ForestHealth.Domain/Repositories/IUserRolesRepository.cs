using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;

namespace ForestHealth.Domain.Repositories
{
    public interface IUserRolesRepository
    {
        UserRoles FetchById(int id);

        IEnumerable<UserRoles> FetchAll();

        IEnumerable<UserRoles> FetchAllRolesByUserInfoId(int userInfoId);

    }
}