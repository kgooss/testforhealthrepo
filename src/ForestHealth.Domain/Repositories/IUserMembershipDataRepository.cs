using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;

namespace ForestHealth.Domain.Repositories
{
    public interface IUserMembershipDataRepository
    { 
        UserMembershipData FetchById(int id);
       
    }
}