using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;

namespace ForestHealth.Domain.Repositories
{
    public interface IUserInfoAggRepository
    {
     
        UserInfoAgg FetchById(int id);
        IEnumerable<UserInfoAgg> FetchAll();

        IEnumerable<UserInfoAgg> FetchAllUsersByOrgStateAbbrAndRoles(string stateAbbr, params string[] roles);

        IEnumerable<UserInfoAgg> FetchAllUsersByEauthId(string eauthId);

    }
}