using ForestHealth.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForestHealth.Domain.Repositories
{
    public interface ILuActivityPriorityRepository
    {
        string InsertAP(string name);
        LuActivityPriority FetchById(string id);
        IEnumerable<LuActivityPriority> FetchAll();
    }
}