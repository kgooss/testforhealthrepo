using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForestHealth.Domain.Entities
{
    public class UserInfo
    {
        [IsIdentifier]
        public int UserInfoId { get; set; }
        public string EauthId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OrganizationUnit { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressCity { get; set; }
        public string AddressZipCode { get; set; }
        public int AddressStateId { get; set; }
        public string Phone { get; set; }
        public string EmailAddress { get; set; }
        public string AreaAffiliation { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public DateTime? DeactivationDate { get; set; }
        public bool AgreedToRules { get; set; }
        public int OrganizationId { get; set; }

    }
}