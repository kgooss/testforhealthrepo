﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForestHealth.Domain.Entities
{
    public class Test
    {
        [IsIdentifier]
        public int TestId { get; set; }
        public string Name { get; set; }
    }
}
