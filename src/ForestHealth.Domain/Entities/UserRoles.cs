using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForestHealth.Domain.Entities
{
    public class UserRoles
    {
        [IsIdentifier]
        public int UserInfoId { get; set; }
        public string Rolename { get; set; }

    }
}