using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForestHealth.Domain.Entities
{
    public class LuStateNonSpatial
    {
        [IsIdentifier]
        public int StateId { get; set; }
        public string Abbreviation { get; set; }
        public string LongName { get; set; }
        public double Area { get; set; }
        public double PriorityArea { get; set; }
        public string RegionName { get; set; }


    }
}