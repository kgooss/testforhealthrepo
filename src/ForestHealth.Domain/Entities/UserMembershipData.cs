using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForestHealth.Domain.Entities
{
    public class UserMembershipData
    {
        [IsIdentifier]
        public int UserInfoId { get; set; }
        public DateTime Lastlogindate { get; set; }
        public DateTime Createdate { get; set; }
    }
}