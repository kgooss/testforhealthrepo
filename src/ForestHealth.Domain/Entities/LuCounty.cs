using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForestHealth.Domain.Entities
{
    public class LuCounty
    {
        [IsIdentifier]
        public int CountyId { get; set; }

        public int StateId { get; set; }

        public string Name { get; set; }
    }
}