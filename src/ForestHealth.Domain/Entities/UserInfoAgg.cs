using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForestHealth.Domain.Entities
{
    public class UserInfoAgg : UserInfo
    {
        public Organization organization { get; set; }

        public UserMembershipData userMembershipData { get; set; }

        public LuStateNonSpatial luStateNonSpatial { get; set; }

        public IEnumerable<UserRoles> userRoles { get; set; }

        public UserInfoAgg(UserInfo userInfo)
        {
            foreach (var property in userInfo.GetType().GetProperties().Where(p => p.SetMethod != null))
                property.SetValue(this, property.GetValue(userInfo));
        }
    }
}