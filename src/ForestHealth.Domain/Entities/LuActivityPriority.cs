using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForestHealth.Domain.Entities
{
    public class LuActivityPriority
    {
        [IsIdentifier]
        public string ActivityPriority { get; set; }
    }
}