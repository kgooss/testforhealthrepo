using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForestHealth.Domain.Entities
{
    public class LuProjectType
    {
        [IsIdentifier]
        public string ProjectType { get; set; }
    }
}