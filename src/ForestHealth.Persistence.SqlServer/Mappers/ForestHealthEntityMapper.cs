﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using ForestHealth.Domain;

namespace ForestHealth.Persistence.SqlServer.Mappers
{
    public class ForestHealthEntityMapper : IMapper
    {
        public ColumnInfo GetColumnInfo(PropertyInfo pocoProperty)
        {
            return new ColumnInfo()
            {
                ColumnName = pocoProperty.Name
            };
        }

        public Func<object, object> GetFromDbConverter(PropertyInfo targetProperty, Type sourceType)
        {
            return null;
        }

        public TableInfo GetTableInfo(Type pocoType)
        {
            var identifierProperties = pocoType.GetProperties()
                .Where(pi => pi.CustomAttributes != null && pi.CustomAttributes.Any(a => a.AttributeType.Equals(typeof(IsIdentifierAttribute))));

            var ti = new TableInfo()
            {
                TableName = pocoType.Name
            };

            if (identifierProperties.Any())
            {
                ti.PrimaryKey = identifierProperties.First().Name;
                ti.AutoIncrement = true;
            }

            return ti;
        }

        public Func<object, object> GetToDbConverter(PropertyInfo sourceProperty)
        {
            return null;
        }
    }
}
