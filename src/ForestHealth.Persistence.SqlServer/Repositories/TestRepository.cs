﻿using ForestHealth.Domain.Repositories;
using ForestHealth.Persistence.SqlServer.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;

namespace ForestHealth.Persistence.SqlServer.Repositories
{
    public class TestRepository : ITestRepository
    {
        public PetaPoco.Database Database { get; set; }

        public TestRepository(IUnitOfWorkFactory unitOfWork)
        {
            this.Database = unitOfWork.CurrentSession;
        }

        public int InsertTest(string name)
        {
            var newTest = new Test()
            {
                Name = name
            };

            this.Database.Insert(newTest);

            return newTest.TestId;
        }

        public Test FetchById(int id)
        {
            return this.Database.Single<Test>(id);
        }

        public IEnumerable<Test> FetchAll()
        {
            return this.Database.Fetch<Test>("WHERE 1=1");
        }
    }
}
