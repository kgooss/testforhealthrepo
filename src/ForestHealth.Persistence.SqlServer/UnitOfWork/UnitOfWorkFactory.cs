﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetaPoco;
using System.Reflection;
using ForestHealth.Domain.Entities;
using ForestHealth.Persistence.SqlServer.Mappers;

namespace ForestHealth.Persistence.SqlServer.UnitOfWork
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        static UnitOfWorkFactory()
        {
            PetaPoco.Mappers.Register(Assembly.GetAssembly(typeof(Test)), new ForestHealthEntityMapper());
        }

        private bool UseTransaction { get; set; }

        public readonly string CurrentSessionKey = Guid.NewGuid().ToString();

        public UnitOfWorkFactory(bool useTransaction)
        {
            this.UseTransaction = useTransaction;
            this.Create();
        }

        private void Create()
        {
            PetaPoco.Database db = new PetaPoco.Database("DefaultConnection");

            if (this.UseTransaction)
                db.BeginTransaction();

            Local.Data[CurrentSessionKey] = db;
        }

        public Database CurrentSession
        {
            get
            {
                return (PetaPoco.Database)Local.Data[CurrentSessionKey];
            }

            set
            {
                Local.Data[CurrentSessionKey] = value;
            }
        }

        public void Dispose()
        {
            if (this.CurrentSession == null)
                return;

            if (this.UseTransaction)
                this.CurrentSession.AbortTransaction();

            this.CurrentSession.Dispose();
            this.CurrentSession = null;
        }
    }
}
