﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForestHealth.Persistence.SqlServer.UnitOfWork
{
    public interface IUnitOfWorkFactory : IDisposable
    {
        PetaPoco.Database CurrentSession { get; set; }
    }
}
