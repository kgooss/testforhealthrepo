﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForestHealth.Persistence.Oracle.UnitOfWork
{
    public interface IUnitOfWorkFactory : IDisposable
    {
        PetaPoco.Database CurrentSession { get; set; }
    }
}
