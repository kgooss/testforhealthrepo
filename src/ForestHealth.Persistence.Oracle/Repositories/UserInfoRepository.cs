using ForestHealth.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;
using ForestHealth.Persistence.Oracle.UnitOfWork;

namespace ForestHealth.Persistence.Oracle.Repositories
{
    public class UserInfoRepository : IUserInfoRepository
    {
        public PetaPoco.Database Database { get; set; }

        public UserInfoRepository(IUnitOfWorkFactory unitOfWork)
        {
            this.Database = unitOfWork.CurrentSession;
        }
        public int InsertUserInfo(string Name, int CountyId, int StateId)
        {
            return -1;
        }

        public UserInfo FetchById(int id)
        {
            return this.Database.Single<UserInfo>(id);
        }

        public IEnumerable<UserInfo> FetchAll()
        {
            return this.Database.Fetch<UserInfo>("WHERE 1=1");
        }

        public int InsertUserInfo(string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<UserInfo> FetchAllUsersByOrgStateAbbrAndRoles(string stateAbbr, params string[] roles)
        {
            throw new NotImplementedException();
        }

       

        public IEnumerable<UserInfo> FetchAllUsersByEauthId(string eauthId)
        {
            return this.Database.Fetch<UserInfo>("WHERE EAUTH_ID = @0", eauthId);
            


            //Labelgen fun
            /*
            IRelationPredicateBucket bucket = new RelationPredicateBucket();
            bucket.Relations.Add(UserInfoEntity.Relations.UserRolesEntityUsingUserInfoId);
            bucket.Relations.Add(UserInfoEntity.Relations.OrganizationEntityUsingOrganizationId);
            bucket.Relations.Add(UserInfoEntity.Relations.UserMembershipDataEntityUsingUserInfoId);

            bucket.PredicateExpression.Add(UserInfoFields.EauthId == eauthId);
            IPrefetchPath2 prefetchPath = new PrefetchPath2(EntityType.UserInfoEntity);
            prefetchPath.Add(UserInfoEntity.PrefetchPathOrganization)
                .SubPath.Add(OrganizationEntity.PrefetchPathLuStateNonSpatial);
            prefetchPath.Add(UserInfoEntity.PrefetchPathUserRoles);
            prefetchPath.Add(UserInfoEntity.PrefetchPathUserMembershipData);
            prefetchPath.Add(UserInfoEntity.PrefetchPathLuStateNonSpatial);

            return this.UserInfoRepository.Get_NoLimit(bucket, prefetchPath).Select(UserInfoAdapter.ConvertEntityToUserInfoWithStateAndRolesDTO);
        */
        }


    }
}