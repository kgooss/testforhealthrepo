using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;
using ForestHealth.Persistence.Oracle.UnitOfWork;
using ForestHealth.Domain.Repositories;

namespace ForestHealth.Persistence.Oracle.Repositories
{
    public class UserMembershipDataRepository : IUserMembershipDataRepository
    {
        public PetaPoco.Database Database { get; set; }

        public UserMembershipDataRepository(IUnitOfWorkFactory unitOfWork)
        {
            this.Database = unitOfWork.CurrentSession;
        }
        
        public UserMembershipData FetchById(int id)
        {
            return this.Database.Single<UserMembershipData>(id);
        }
    }
}