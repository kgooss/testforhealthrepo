using ForestHealth.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;
using ForestHealth.Persistence.Oracle.UnitOfWork;

namespace ForestHealth.Persistence.Oracle.Repositories
{
    public class UserRolesRepository : IUserRolesRepository
    {
        public PetaPoco.Database Database { get; set; }

        public UserRolesRepository(IUnitOfWorkFactory unitOfWork)
        {
            this.Database = unitOfWork.CurrentSession;
        }
        public UserRoles FetchById(int id)
        {
            return this.Database.Single<UserRoles>(id);
        }

        public IEnumerable<UserRoles> FetchAll()
        {
            return this.Database.Fetch<UserRoles>("WHERE 1=1");
        }
        
        public IEnumerable<UserRoles> FetchAllRolesByUserInfoId(int userInfoId)
        {
            return this.Database.Fetch<UserRoles>("WHERE USER_INFO_ID = @0", userInfoId);
        }
    }
}