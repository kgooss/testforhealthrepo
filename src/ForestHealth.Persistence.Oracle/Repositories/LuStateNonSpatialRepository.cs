using ForestHealth.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;
using ForestHealth.Persistence.Oracle.UnitOfWork;

namespace ForestHealth.Persistence.Oracle.Repositories
{
    public class LuStateNonSpatialRepository : ILuStateNonSpatialRepository
    {
        public PetaPoco.Database Database { get; set; }

        public LuStateNonSpatialRepository(IUnitOfWorkFactory unitOfWork)
        {
            this.Database = unitOfWork.CurrentSession;
        }

        public LuStateNonSpatial FetchById(int id)
        {
            LuStateNonSpatial lu = this.Database.SingleOrDefault<LuStateNonSpatial>(id);
            if (lu == null)
            {
                return new LuStateNonSpatial() { Abbreviation="", StateId = 0 };
            }
            return this.Database.SingleOrDefault<LuStateNonSpatial>(id);
        } 
    }
}