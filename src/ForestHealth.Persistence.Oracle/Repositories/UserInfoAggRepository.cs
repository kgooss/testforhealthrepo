using ForestHealth.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;
using ForestHealth.Persistence.Oracle.UnitOfWork;

namespace ForestHealth.Persistence.Oracle.Repositories
{
    public class UserInfoAggRepository : IUserInfoAggRepository
    {
        public PetaPoco.Database Database { get; set; }

        public IOrganizationRepository OrgRepo { get; set; }
        public IUserMembershipDataRepository MemRepo { get; set; }
        public ILuStateNonSpatialRepository StateRepo { get; set; }
        public IUserRolesRepository RoleRepo { get; set; }

        public UserInfoAggRepository(IUnitOfWorkFactory unitOfWork)
        {
            this.Database = unitOfWork.CurrentSession;
        }

        public UserInfoAgg FetchById(int id)
        {
            return this.Database.Single<UserInfoAgg>(id);
        }

        public IEnumerable<UserInfoAgg> FetchAll()
        {
            return this.Database.Fetch<UserInfoAgg>("WHERE 1=1");
        }

        public int InsertUserInfoAgg(string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<UserInfoAgg> FetchAllUsersByOrgStateAbbrAndRoles(string stateAbbr, params string[] roles)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<UserInfoAgg> FetchAllUsersByEauthId(string eauthId)
        {
            //userInfoAgg derives from UserInfo but adds a few other fields from joined tables
            List<UserInfoAgg> availableUsersAgg = new List<UserInfoAgg>();
            //pull the base UserInfos
            var availableUsers = this.Database.Fetch<UserInfo>("WHERE EAUTH_ID = @0", eauthId);

            availableUsersAgg = availableUsers.Select(u =>
            {
                var org = OrgRepo.FetchById(u.OrganizationId);
                var mem = MemRepo.FetchById(u.UserInfoId);
                var state = StateRepo.FetchById(u.AddressStateId);
                var roles = RoleRepo.FetchAllRolesByUserInfoId(u.UserInfoId);

                UserInfoAgg uAgg = new UserInfoAgg(u);
                uAgg.organization = org;
                uAgg.userMembershipData = mem;
                uAgg.luStateNonSpatial = state;
                uAgg.userRoles = roles;

                return uAgg;
            }).ToList();

            return availableUsersAgg;
        }

        public int InsertUserInfo(string name)
        {
            throw new NotImplementedException();
        }
    }
}