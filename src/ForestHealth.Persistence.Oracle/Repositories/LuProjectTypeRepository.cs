using ForestHealth.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;
using ForestHealth.Persistence.Oracle.UnitOfWork;

namespace ForestHealth.Persistence.Oracle.Repositories
{
    public class LuProjectTypeRepository : ILuProjectTypeRepository
    {
        public PetaPoco.Database Database { get; set; }

        public LuProjectTypeRepository(IUnitOfWorkFactory unitOfWork)
        {
            this.Database = unitOfWork.CurrentSession;
        }

        public LuProjectType FetchById(string id)
        {
            return this.Database.SingleOrDefault<LuProjectType>(id);
        } 

        public IEnumerable<LuProjectType> FetchAll()
        {
            return this.Database.Fetch<LuProjectType>("WHERE 1=1");
        }
    }
}