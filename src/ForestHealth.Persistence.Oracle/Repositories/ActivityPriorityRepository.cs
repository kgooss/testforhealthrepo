using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;
using ForestHealth.Domain.Repositories;
using ForestHealth.Persistence.Oracle.UnitOfWork;

namespace ForestHealth.Persistence.Oracle.Repositories
{
    public class ActivityPriorityRepository: ILuActivityPriorityRepository
    {
        public PetaPoco.Database Database { get; set; }

        public ActivityPriorityRepository(IUnitOfWorkFactory unitOfWork)
        {
            this.Database = unitOfWork.CurrentSession;
        }
        public string InsertAP(string ap)
        {
            var newAP = new LuActivityPriority()
            {
                ActivityPriority = ap
            };

            this.Database.Insert(newAP);

            return newAP.ActivityPriority;
        }

        public LuActivityPriority FetchById(string id)
        {
            return this.Database.Single<LuActivityPriority>(id);
        }

        public IEnumerable<LuActivityPriority> FetchAll()
        {
            return this.Database.Fetch<LuActivityPriority>("WHERE 1=1");
        }
    }
}