using ForestHealth.Domain.Entities;
using ForestHealth.Domain.Repositories;
using ForestHealth.Persistence.Oracle.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForestHealth.Persistence.Oracle.Repositories
{
    public class CountyRepository : ILuCountyRepository
    {
        public PetaPoco.Database Database { get; set; }

        public CountyRepository(IUnitOfWorkFactory unitOfWork)
        {
            this.Database = unitOfWork.CurrentSession;
        }
        public int InsertCounty(string Name, int CountyId, int StateId)
        {
            var newCounty = new LuCounty()
            {
                Name = Name,
                CountyId = CountyId,
                StateId = StateId
            };

            this.Database.Insert(newCounty);

            return newCounty.CountyId;
        }

        public LuCounty FetchById(int id)
        {
            return this.Database.Single<LuCounty>(id);
        }

        public IEnumerable<LuCounty> FetchAll()
        {
            return this.Database.Fetch<LuCounty>("WHERE 1=1");
        }
    }
}