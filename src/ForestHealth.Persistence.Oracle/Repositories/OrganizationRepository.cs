using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForestHealth.Domain.Entities;
using ForestHealth.Persistence.Oracle.UnitOfWork;
using ForestHealth.Domain.Repositories;

namespace ForestHealth.Persistence.Oracle.Repositories
{
    public class OrganizationRepository : IOrganizationRepository
    {
        public PetaPoco.Database Database { get; set; }

        public OrganizationRepository(IUnitOfWorkFactory unitOfWork)
        {
            this.Database = unitOfWork.CurrentSession;
        }

        public Organization FetchById(int id)
        {
            return this.Database.Single<Organization>(id);
        }

        public IEnumerable<Organization> FetchAll()
        {
            return this.Database.Fetch<Organization>("WHERE 1=1");
        }

        public IEnumerable<Organization> FetchAllFSRegions()
        {
            return this.Database.Fetch<Organization>("WHERE STATE_ID IS NULL");
        }

        
    }
}