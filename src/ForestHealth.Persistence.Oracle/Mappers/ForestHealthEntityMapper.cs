﻿using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using ForestHealth.Domain;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Configuration;

namespace ForestHealth.Persistence.Oracle.Mappers
{
    public class ForestHealthEntityMapper : IMapper
    {
        private static readonly NameValueCollection OracleOverrides;

        static ForestHealthEntityMapper()
        {
            OracleOverrides = ConfigurationManager.GetSection("oracleSchemaNameOverwrites") as NameValueCollection;
        }
        
        public Func<object, object> GetFromDbConverter(PropertyInfo targetProperty, Type sourceType)
        {
            if (targetProperty.PropertyType.Equals(typeof(bool)))
                return (obj) => obj.ToString() == "Y";

            if (targetProperty.PropertyType.Equals(typeof(int?)) && sourceType.Equals(typeof(decimal)))
                return (obj) =>
                {
                    if (obj == null)
                        return (int?)null;

                    return (int)((decimal)obj);
                };

            if (sourceType.Equals(typeof(float)))
                return (obj) => (decimal)((float)obj);

            return null;
        }

        public TableInfo GetTableInfo(Type pocoType)
        {
            var ti = new TableInfo()
            {
                TableName = pocoType.Name
            };

            if (!pocoType.GetCustomAttributes(typeof(TableNameAttribute), true).Any())
            {
                // create a table name from the entity that conforms to the oracle naming convention
                ti.TableName = ForestHealthEntityMapper.ConvertPascalCaseToOracleCase(ti.TableName);
                ti.TableName = ti.TableName.Replace("_DTO", "");
            }

            var identifierProperties = pocoType.GetProperties()
                .Where(pi => pi.CustomAttributes != null && pi.CustomAttributes.Any(a => a.AttributeType.Equals(typeof(IsIdentifierAttribute))));

            if (identifierProperties != null && identifierProperties.Count() > 0)
            {
                ti.PrimaryKey = ForestHealthEntityMapper.ConvertPascalCaseToOracleCase(identifierProperties.First().Name);
                ti.AutoIncrement = true;
            }

            return ti;
        }

        public static string GetOracleSchemaOverride()
        {
            return OracleOverrides.Count > 0 ? OracleOverrides.Get(0) : "";
        }

        public Func<object, object> GetToDbConverter(PropertyInfo sourceProperty)
        {
            if (sourceProperty.Equals(typeof(bool)))
                return (obj) => ((bool)obj) ? 'Y' : 'N';

            return null;
        }

        public ColumnInfo GetColumnInfo(PropertyInfo pocoProperty)
        {
            return new ColumnInfo()
            {
                ColumnName = ForestHealthEntityMapper.ConvertPascalCaseToOracleCase(pocoProperty.Name)
            };
        }

        public bool MapPropertyToColumn(System.Reflection.PropertyInfo pi, ref string columnName, ref bool resultColumn)
        {
            columnName = ForestHealthEntityMapper.ConvertPascalCaseToOracleCase(columnName);
            return true;
        }

        private static string ConvertPascalCaseToOracleCase(string name)
        {
            name = new Regex("([A-Z]+[a-z0-9]*)").Replace(name.Replace("\"", ""), m => m.Value + "_");
            return name.Substring(0, name.Length - 1).ToUpper();
        }
    }
}